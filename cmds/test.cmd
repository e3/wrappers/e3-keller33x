require keller33x

epicsEnvSet("MAXWAV", "100")

dbLoadRecords("convert.template", "P=TEST, R=:, MAXWAV=$(MAXWAV), INWAV1=PRL:ec0-s179-EL6022-Serial1-ArrayIn, OUTWAV1=PRL:ec0-s179-EL6022-Serial1-ArrayOut, INWAV2=PRL:ec0-s179-EL6022-Serial2-ArrayIn, OUTWAV2=PRL:ec0-s179-EL6022-Serial2-ArrayOut")
